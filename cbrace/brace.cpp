#include <iostream>
#include <vector>
#include <regex>
#include "tokens.cpp"
#include "expressions.cpp"
using namespace std;

class Scanner {
    private:
        const string source;
        vector<Token*> tokens;
        int start;
        int current;
        Cursor cursor = Cursor();
        vector<Token*> tempTokens;
        string tempString;
        vector<string> messages;
        int restart(){
            cursor.restart();
            start = current;
            return(start);
        }
        int advance(){
            cursor.next();
            current++;
            return(current - 1);
        }
        bool isAtEnd(){
            return current >= (source.length() - 1);
        }
        string currentString(int offset = 0){
            return(source.substr(start, (current - start) + 1 + offset));
        }
        void queueToken(){
            tempString = tempString + currentString();
        }
        void queueToken(TokenType type){
            tempString = tempString + currentString();
            tempTokens.push_back(new Token(type, currentString(),cursor.seal()));
        }
        void queueToken(TokenType type, string literal){
            tempString = tempString + literal;
            tempTokens.push_back(new Token(type, literal,cursor.seal()));
        }
        void addToken(TokenType type){
            tokens.push_back(new Token(type, currentString(),cursor.seal()));
        }
        void addToken(TokenType type, string literal){
            tokens.push_back(new Token(type, literal,cursor.seal()));
        }
        void consolidate(){
            tokens.insert(
                tokens.end(),
                std::make_move_iterator(tempTokens.begin()),
                std::make_move_iterator(tempTokens.end())
            );
            tempTokens.clear();
            tempString = "";
        }
        void fallback(){
            tokens[tokens.size() - 1]->extend(tempString,cursor.seal());
            tempTokens.clear();
            tempString = "";
        }
        bool match(char expected){
            if (isAtEnd()){ return(false); }
            if (source[current + 1] != expected){ return(false); }
            advance();
            return(true);
        }
        bool scanKey(){
            if (regex_match(source.substr(current, 1), regex("[\\._a-zA-Z]"))){
                while (regex_match(source.substr(current + 1, 1), regex("[\\._a-zA-Z0-9 ]"))){
                    advance();
                }
                return(true);
            } else {
                return(false);
            }
        }
        void scanComment(){
            while (current < source.length()){
                if (match('/')){
                    return;
                } else if (match(' ')){
                    cursor.indent();
                } else if (match('\t')){
                    cursor.indent();
                } else if (match('\n')){
                    cursor.line();
                } else {
                    advance();
                }
            }
        }
        bool scanLiteral(){
            string literal = "";
            while (current < source.length()){
                if (source[current] == '{'){
                    if (match('{')){
                        literal = literal + source[advance()];
                    } else {
                        addToken(LITERAL, literal);
                        restart();
                        queueToken(BCOPEN);
                        return(false);
                    }
                } else {
                    if (source[current] == ' ' || source[current] == '\t'){
                        cursor.indent();
                    } else if (source[current] == '\n'){
                        cursor.line();
                    }
                    literal = literal + source[advance()];
                }
            }
            addToken(LITERAL, literal);
            return(true);
        }
        bool scanToken(){
            char c = source[current];
            switch(c){
                case '{' :
                    fallback();
                    queueToken(BCOPEN);
                    return(false);
                case '}' :
                    queueToken(BCCLOSE);
                    consolidate();
                    return(true);
                    break;
                case '?' : queueToken(QUEST); return(false);
                case '@' : queueToken(ATSIGN); return(false);
                case '[' : queueToken(BKOPEN); return(false);
                case ']' : queueToken(BKCLOSE); return(false);
                case '|' :
                        queueToken(PIPE);
                    return(false);
                case ':' :
                    if (match('!')){
                        queueToken(COLONNOT);
                    } else {
                        queueToken(COLON);
                    }
                    return(false);
                case ';' : queueToken(SEMICOLON); return(false);
                case '(' : queueToken(PROPEN); return(false);
                case ')' : queueToken(PRCLOSE); return(false);
                case '=' :
                    if (match('!')){
                        queueToken(EQUALNOT);
                    } else {
                        queueToken(EQUAL);
                    }
                    return(false);
                case '<' :
                    if (match('!')){
                        queueToken(NGOPENNOT);
                    } else if (match('<')){
                        if (match('!')){
                            queueToken(NGNGOPENNT);
                        } else {
                            queueToken(NGNGOPEN);
                        }
                    } else {
                        queueToken(NGOPEN);
                    }
                    return(false);
                case '>' :
                    if (match('>')){
                        queueToken(NGNGCLOSE);
                    } else {
                        queueToken(NGCLOSE);
                    }
                    return(false);
                case '*' : queueToken(STAR); return(false);
                case '+' : queueToken(PLUS); return(false);
                case '^' : queueToken(CARET); return(false);
                case '/' :
                    scanComment();
                    queueToken();
                    return(false);
                case ' ' :
                    cursor.indent();
                case '\r' :
                case '\t' :
                    cursor.indent();
                case '\n' :
                    cursor.line();
                    return(false);
                default:
                    if (scanKey()){
                        queueToken(KEY);
                        return(false);
                    } else {
                        queueToken(ERR);
                        logMessage(current, c, "Illegal character.");
                        return(false);
                    }
            }
        }
        void logMessage(int pos, char c, string message){
            message += ("At position: " + pos);
            message += (" character:" + c);
            messages.push_back(message);
        }
    public:
        Scanner(string expr) : source(expr) {
            current = 0;
            restart();
            tempString = "";
            bool isLiteral = true;
            while(current < source.length()){
                if (isLiteral){
                    isLiteral = scanLiteral();
                } else {
                    isLiteral = scanToken();
                }
                advance();
                restart();
            }
            if (!isLiteral){
                fallback();
            }
            tokens.push_back(new Token(EOS, "",cursor.seal()));
        };
        void print(){
            for (int i = 0; i < tokens.size(); i++){
                cout << tokens[i]->toString() << endl;
            }
        }
        vector<Token*> getTokens(){
            return(tokens);
        }
};

class Parser {
    private:
        const vector<Token*> tokens;
        int current = 0;
        vector<string> messages;
        bool isAtEnd(){
            return tokens[current]->match({TokenType::EOS});
        }
        bool match(vector<TokenType> types){
            return(tokens[current]->match(types));
        }
        void syncronize(){
            while (!isAtEnd()){
                current++;
                if (match({BCCLOSE})){
                    current++;
                    return;
                }
            }
        }
        exception error(Token* token, string message){
            logMessage(current, token, message);
            return(exception());
        }
        Step readStep(){

        }
        Path readPath(){
            Step left = readStep();
            vector<pair<NextType, Step>> pairs;
            NextType nextType;
            while (match({NGCLOSE, NGNGCLOSE})){
                if (match({NGCLOSE})){
                    nextType = NXT_NG;
                } else {
                    nextType = NXT_NGNG;
                }
                pairs.push_back({
                    nextType, 
                    readStep()
                });
            }
            return(Path(left, pairs));
        }
        Statement readStatement(){
            Prefix prefix;
            Postfix postfix;
            current ++;
            if (match({NGCLOSE})){
                prefix = PRE_NG;
            } else if (match({NGNGCLOSE})){
                prefix = PRE_NGNG;
            } else if (match({QUEST})){
                prefix = PRE_QUEST;
            } else if (match({KEY})){
                prefix = PRE_NONE;
            } else {
                throw error(tokens[current], "Illegal brace expression start. ");
                syncronize();
            }
            Path path = readPath();
            if (match({STAR})){
                postfix = PST_STAR;
            } else if (match({PLUS})){
                postfix = PST_PLUS;
            } else if (match({CARET})){
                postfix = PST_CARET;
            } else if (match({BCCLOSE})){
                postfix = PST_NONE;
            } else {
                throw error(tokens[current], "Illegal brace expression end. ");
                syncronize();
            }
            return(Statement(prefix, path, postfix));
        }
        string readLiteral(){
            return(tokens[current++]->toString());
        }
        Text readText(){
            string left = readLiteral();
            vector<pair<Statement, string>> pairs;
            // need to try catch readStatement calls
            //  in the catch, discard statement and append next literal to last literal
            while (!isAtEnd()){
                pairs.push_back({
                    readStatement(),
                    readLiteral()
                });
            }
            return Text(left, pairs);
        }
        void logMessage(int pos, Token* token, string message){
            message += ("At position: " + pos);
            message += (" token:" + token->toString());
            messages.push_back(message);
        }
    public:
        Parser(vector<Token*> source = {}) : tokens(source){

        }
};

class Brace {
    private:
        Scanner* scanner;
        Parser* parser;
        bool hadError;
    public:
        void scanErr(int pos, string message){
            cerr << "Scanning Error at position " << pos << ": " << message;
            hadError = true;
        }
        Brace(string source){
            scanner = new Scanner(source);
            parser = new Parser(scanner->getTokens());
        }
};

int main(int argc, char* argv[]) {
    if (argc == 1){
        cout << "No Arguments." << endl;
        return 0;
    }
    Brace brace = Brace(string(argv[1]));
}