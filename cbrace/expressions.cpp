#include <string>
#include <vector>
#include <tuple>
using namespace std;

class Expression {
    public:
        string toString(){
            return("TODO");
        }
};

class Identifier : public Expression {
    private:
        string text;
    public:
        Identifier(string x) : text(x) {}
};

enum FilterType {
    FLT_EQUAL,
    FLT_EQUALNOT,
    FLT_COLON,
    FLT_COLONNOT
};
class Filter : public Expression {
    private:
        FilterType filtertype;
        Identifier identifier;
    public:
        Filter(FilterType x, Identifier y) : filtertype(x), identifier(y) {}
};

class Key : public Expression {
    private:
        string identifier;
    public:
        Key(string x) : identifier(x) {}
};

enum LookupType {
    LU_NG,
    LU_NGNOT,
    LU_NGNG,
    LU_NGNGNOT
};
class Lookahead : public Expression {
    private:
        LookupType lookup;
        Key key;
        Filter filter;
    public:
        Lookahead(LookupType x, Key y, Filter z) : lookup(x), key(y), filter(z) {}
};

class Step : public Expression {
    private:
        string alias;
        Key key;
        Filter* filter;
        Lookahead* lookahead;
    public:
        Step(string x, Key y, Filter* z, Lookahead* w) : alias(x), key(y), filter(z), lookahead(w) {}

};

enum NextType {
    NXT_NG,
    NXT_NGNG
};
class Path : public Expression {
    private:
        Step start;
        vector<pair<NextType, Step>> pairs;
    public:
        Path(Step x, vector<pair<NextType, Step>> y) : start(x), pairs(y) {}
        string toString(){
            string output = start.toString();
            for (int i = 0; i < pairs.size(); i++){
                output = output + "\n\t";
                switch(pairs[i].first){
                    case NXT_NG:
                        output = output + ">"; break;
                    case NXT_NGNG:
                        output = output + ">>"; break;
                }
                output = output + pairs[i].second.toString();
            }
            return(output);
        }
};

enum Prefix {
    PRE_QUEST,
    PRE_NG,
    PRE_NGNG,
    PRE_NONE
};
enum Postfix {
    PST_STAR,
    PST_CARET,
    PST_PLUS,
    PST_NONE
};
class Statement : public Expression {
    private:
        Prefix prefix;
        Path path;
        Postfix postfix;
    public:
        Statement(Prefix x, Path y, Postfix z) : prefix(x), path(y), postfix(z) {}
        string toString() {
            string output = "{\n\t";
            switch(prefix){
                case PRE_QUEST:
                    output = output + "?"; break;
                case PRE_NG:
                    output = output + ">"; break;
                case PRE_NGNG:
                    output = output + ">>"; break;
            }
            output = output +  path.toString();
            switch(postfix){
                case PST_STAR:
                    output = output + "?"; break;
                case PST_CARET:
                    output = output + ">"; break;
                case PST_PLUS:
                    output = output + ">>"; break;
            }
            output = output + "\n}";
            return(output);
        }
};

class Text : public Expression {
    private:
        string start;
        vector<pair<Statement, string>> pairs;
    public:
        Text(string x, vector<pair<Statement, string>> y) : start(x), pairs(y) {}
        string toString(){
            string output = start + "\n";
            for (int i = 0; i < pairs.size(); i++){
                output = output + pairs[i].first.toString() + "\n" + pairs[i].second;
            }
            return(output);
        }
};