#include <string>
#include <vector>
using namespace std;

enum TokenType {
    //TODO: Casting %...%
    //TODO: Aggregating #...#
    //TODO: In Literals [...]
    BCOPEN,     //  {
    BCCLOSE,    //  }
    QUEST,      //  ?
    ATSIGN,     //  @
    BKOPEN,     //  [
    BKCLOSE,    //  ]
    PIPE,       //  |
    COLON,      //  :
    COLONNOT,   //  :!
    SEMICOLON,  //  ;
    PROPEN,     //  (
    PRCLOSE,    //  )
    EQUAL,      //  =
    EQUALNOT,   //  =!
    NGOPEN,     //  <
    NGOPENNOT,  //  <!
    NGNGOPEN,   //  <<
    NGNGOPENNT, //  <<!
    NGCLOSE,    //  >
    NGNGCLOSE,  //  >>
    STAR,       //  *
    PLUS,       //  +
    CARET,      //  ^
    KEY,
    LITERAL,
    ERR,
    EOS
};

struct Position {
    int line;
    int indent;
    int shift;
};
class Span {
    protected:
        Position first;
        Position last;
        bool singlet = true;
    public:
        Span(Position _first = {0, 0, 0}, Position _last = {0, 0, 0}) : first(_first), last(_last) {
            if (_first.line != _last.line || _first.shift != _last.shift){
                singlet = false;
            }
        }
        Position getFirst(){
            return(first);
        }
        Position getLast(){
            return(last);
        }
        void join(Span span){
            last = span.getLast();
        }
};
class Cursor : public Span {
    public:
        Cursor(int _line = 0, int _indent = 0, int _shift = 0){
            first = {_line, _indent, _shift};
            last = {_line, _indent, _shift};
        }
        void next(){
            last.shift++;
        }
        void line(){
            last.line++;
            last.shift = 0;
            last.indent = 0; 
        }
        void indent(){
            last.shift || last.indent++;
        }
        void restart(){
            first = {last.line, last.indent, last.shift}; 
            last = {last.line, last.indent, last.shift};
        }
        Span seal(){
            return(Span(first, last));
        }
};
class Token {
    private:
        const TokenType type;
        string literal;
        Span span;
    public:
        Token(const TokenType _type, string _literal, Span _span) : type(_type), literal(_literal), span(_span) {}
        string toString() const {
            return literal;
        }
        void extend(string extension, Span _span){
            literal = literal + extension;
            span.join(_span);
        }
        bool match(vector<TokenType> types) const {
            for (int i = 0; i < types.size(); i++){
                if (type == types[i]){
                    return (true);
                }
            }
            return (false);
        }
};